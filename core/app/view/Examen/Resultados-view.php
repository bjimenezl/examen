<?php $Examen = UsuarioExamenData::getByIdExamen($_POST["id"]); ?>
<!-- Scrollable Table Start -->
<div class="col-md-12 col-lg-12">
	<div class="card mg-b-20">
		<div class="card-header">
			<h4 class="card-header-title">
				preguntas
			</h4>
			<div class="card-header-btn">
				<a href="#" data-toggle="collapse" class="btn card-collapse" data-target="#collapse7" aria-expanded="true"><i class="ion-ios-arrow-down"></i></a>
				<a href="#" data-toggle="refresh" class="btn card-refresh"><i class="ion-android-refresh"></i></a>
				<a href="#" data-toggle="expand" class="btn card-expand"><i class="ion-android-expand"></i></a>
				<a href="#" data-toggle="remove" class="btn card-remove"><i class="ion-android-close"></i></a>
			</div>
		</div>
		<div class="card-body pd-0 collapse show" id="productSalesDetails">
			<div class="table-responsive">
				<table id="dtHorizontalVerticalExample" class="table table-striped table-bordered table-sm" cellspacing="0" width="100%">
					<thead class="thead-colored thead-success">
						<tr>
					
                        <th>Calificacion</th>
                        <th>Usuario</th>
						
						</tr>
					</thead>
					<tbody>
						<?php
						foreach ($Examen as $Examen) :

                            $Calificacion=CalificacionData::getByIdExamen($Examen->id);
                            $Usuario=UserData::getById($Examen->id_usuario);
						?>
							<tr>
								<td><?php echo $Calificacion->nota; ?></td>
                                <td><?php echo $Usuario->nombre; ?></td>
                               
							
							</tr>
						<?php
						endforeach;
						?>
					</tbody>
					<tfoot class="thead-colored thead-success">
						<tr>
                      
                        <th>Calificacion</th>
                        <th>Usuario</th>
					
						</tr>
					</tfoot>
				</table>
			</div>
		</div>
	</div>
</div>	
	<!--/ Scrollable Table End -->