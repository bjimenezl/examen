<?php
class UsuarioExamenData
{
	public static $tablename = "usuarios_examen"; //id_examen EN LA BASE DE DATOS 


	public function UsuarioExamenData()
	{
		$this->id = "";
		$this->id_examen = null;
        $this->id_usuario = null;
		$this->estado = 0;
		
	}



	public function add()
	{
		$sql = "insert into " . self::$tablename . " (id_examen,id_usuario,estado) ";
		$sql .= "value (\"$this->id_examen\",\"$this->id_usuario\",\"$this->estado\")";
		return Executor::doit($sql);
	}



	public static function delById($id)
	{
		$sql = "delete from " . self::$tablename . " where id=$id";
		return	Executor::doit($sql);
	}
	public function del()
	{
		$sql = "delete from " . self::$tablename . " where id=$this->id";
		return	Executor::doit($sql);
	}

	// partiendo de que ya tenemos creado un objecto ClienteData previamente utilizamos el contexto


	public function TerminarExamen(){
		$sql = "update ".self::$tablename." set 
        estado=1 
        where id=$this->id";
		return Executor::doit($sql);
	}



	public static function getById($id)
	{
		$sql = "select * from " . self::$tablename . " where id=$id";
		$query = Executor::doit($sql);
		return Model::one($query[0], new UsuarioExamenData());
	}

	public static function getByIdExamen($id)
	{
		$sql = "select * from " . self::$tablename . " where id_examen='" . $id . "' and estado=1";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UsuarioExamenData());
	}



	public static function getAll()
	{
		$sql = "select * from " . self::$tablename;
		$query = Executor::doit($sql);
		return Model::many($query[0], new UsuarioExamenData());
	}


	public static function getLike($q)
	{
		$sql = "select * from " . self::$tablename . " where id_examen like '%$q%' or cc like '%$q%'";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UsuarioExamenData());
	}

	public static function getByIdUser($id)
	{
		$sql = "select * from " . self::$tablename . " where id_usuario=$id ";
		$query = Executor::doit($sql);
		return Model::many($query[0], new UsuarioExamenData());
	}


}
